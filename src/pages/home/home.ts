import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization';
import { FileChooser } from '@ionic-native/file-chooser';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tlanguage:any;
  laguagelist:any;

  constructor(public navCtrl: NavController,private file: File,private fileChooser: FileChooser,private fileOpener: FileOpener,public platform: Platform, public translateService: TranslateService,private globalization: Globalization) {
   /* this.tlanguage="en";
    if(this.tlanguage == 'en'){
      this.translateService.use(this.tlanguage);
    }*/

    this.platform.ready().then(() => {
      this.globalization.getPreferredLanguage()
      .then(res => {
        console.log(res.value);
    /* if(res.value == 'en-AU' || res.value == 'en-BZ' || res.value == 'en-CA' || res.value == 'en-CB' || res.value == 'en-GB' || res.value == 'en-IN' || res.value == 'en-IE' || res.value == 'en-JM' || res.value == 'en-NZ' || res.value == 'en-PH' || res.value == 'en-ZA' || res.value == 'en-TT' || res.value == 'en-US' ){
          this.tlanguage="en";
          this.translateService.use(this.tlanguage);
        }else if(res.value == 'it-IT' ){
          this.tlanguage="sp";
          this.translateService.use(this.tlanguage);
        }else if(res.value == 'fr-FR'){
          this.tlanguage="ar";
          this.translateService.use(this.tlanguage);
        }else{
          this.tlanguage="ar";
          this.translateService.use(this.tlanguage);
        }*/
        if(res.value == 'en-AU' || res.value == 'en-BZ' || res.value == 'en-CA' || res.value == 'en-CB' || res.value == 'en-GB' || res.value == 'en-IN' || res.value == 'en-IE' || res.value == 'en-JM' || res.value == 'en-NZ' || res.value == 'en-PH' || res.value == 'en-ZA' || res.value == 'en-TT' || res.value == 'en-US' ){
          this.tlanguage="en";
          this.translateService.use(this.tlanguage);
        }else if(res.value == 'es-AR' || res.value == 'es-BO' || res.value == 'es-CI' || res.value == 'es-CO' || res.value == 'es-CR' || res.value == 'es-DO' || res.value == 'es-EC' || res.value == 'es-SV' || res.value == 'es-GT' || res.value == 'es-HN' || res.value == 'es-MX' || res.value == 'es-NI' || res.value == 'es-PA' || res.value == 'es-PY' || res.value == 'es-PE' || res.value == 'es-PR' || res.value == 'es-ES' || res.value == 'es-UY' || res.value == 'es-UY'){
          this.tlanguage="sp";
          this.translateService.use(this.tlanguage);
        }else if(res.value == 'ar-dz' || res.value == 'ar-BH' || res.value == 'ar-EG' || res.value == 'ar-IQ' || res.value == 'ar-JO' || res.value == 'ar-KW' || res.value == 'ar-LB' || res.value == 'ar-LY'|| res.value == 'ar-MA' || res.value == 'ar-OM' || res.value == 'ar-QA' || res.value == 'ar-SA' || res.value == 'ar-SY' || res.value == 'ar-TN' || res.value == 'ar-AE' || res.value == 'ar-YE'){
          this.tlanguage="ar";
          this.translateService.use(this.tlanguage);
        }else{
          this.tlanguage="en";
          this.translateService.use(this.tlanguage);
        }
      })
      .catch(e => console.log(e));
     })

  }
  ngOnInit(){
    this.laguagelist=[{id:"1" ,name:"english",value:"en"},{id:"2",name:"spanish",value:"sp"},{id:"3" ,name:"Arabic",value:"ar"}];
    
  }

  public _robotOverlords: any[] = [];
  url:any;
  test(){
    this.fileChooser.open()
    .then(uri =>{
      console.log(uri);
     }) 
    .catch(e => console.log(e));
  }


  /*changeLanguage(langauge) {
    this.translateService.use(langauge);
  }*/


  multiLangSel(){
    console.log(this.tlanguage);
    if(this.tlanguage == 'en'){
      this.translateService.use(this.tlanguage);
      //localStorage.setItem("language",this.tlanguage);
    }
    else if(this.tlanguage == 'sp'){
      this.translateService.use(this.tlanguage);
      //localStorage.setItem("language",this.tlanguage)
    }
    else if(this.tlanguage == 'ar'){
      this.translateService.use(this.tlanguage);
      //localStorage.setItem("language",this.tlanguage)
    }
  }

  
}
